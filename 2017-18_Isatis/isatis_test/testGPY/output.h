

void printContext(Stream& stream);
void printData(int i, double outputV, double dustDensity, unsigned long readingDuration, bool quality, Stream& stream);
void printResults(Stream &stream);
