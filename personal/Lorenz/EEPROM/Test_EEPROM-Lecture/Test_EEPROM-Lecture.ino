#include "EEPROM.h"
#define Phrase_Lecture

int addr = 0;
const int Ecriture = 1000;

void setup() {
  Serial.begin(9600);

#ifdef Entier_Lecture
  int NB = EEPROM.read(addr);
  Serial.print(NB);
#endif

#ifdef Phrase_Lecture
  char buffer[12];
  EEPROM.get(addr, buffer);
  Serial.println(buffer);
#endif

#ifdef Rationnel_Lecture
  float PP = EEPROM.get(addr, PP);
  Serial.println(PP);
#endif

  delay(Ecriture);
}

void loop() {

}
