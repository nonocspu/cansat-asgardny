/*
 * TemperatureClient.h
 */

#pragma once

#include "Arduino.h"
#include "IsaTwoRecord.h"

/** @ingroup IsaTwoCSPU
 * @brief Client class for temperature measurement with  NTC thermistor NTCLE203E3103GB0 from VISHAY BC.
 *
 * For reference, it was compared to:
 * - a NTC thermistor TT3-10KC3-AG10
 * - a thermocouple type T with the Adafruit amplifier board XXXX
 * - a thermocouple type K with the Adafruit amplifier board YYYY
 * - a LM35
 * - a CMCJU7420
 * - a DS18B20
 * The chosen NTC showed the fastest response time and a fairly good precision.
 * The idea is to use a voltage divider composed by the thermistor and a known resistor
 * with a value near the thermistor value during the measurements.
 */
class TemperatureClient {
public:
	/**
	 * The resistance of the NTC thermistor is measured via a voltage measurement
	 * on an analog pin of the arduino.
	 * @param pinNumber the number of the pin from which the analog_read will be done,
	 * 		  which should be the Arduino pin binded to the center of the voltage divider
	 */
	TemperatureClient(byte pinNumber);

  
  void begin(float theReferenceVoltage,int theNumADC_Steps);
   
	/**
	 * Acquire the measure.. The temperature measured is stored in a project-defined class.
	 * @param record The IsaTwoRecord to complete.
	 */
	bool readData(IsaTwoRecord& record);

private:
	byte pinNumber_; /**< The pin number binded to the central point of the tension divider  */
  float refVoltage;
  int numADC_Steps; 
  
	/** Function to translate the NTC resistance to the temperature
	 *  It converts the resistance in temperature based on the table of conversion found in
	 *  the datasheet of the NTC.
	 *  The table has been converted in a contiuous exponential function on the domain
	 *  of measurement by interpolation of the discrete values.
	 *  @param resistance Holds the resistance in ohm to be converted in °C
	 */
	float translation(float resistance);
};
