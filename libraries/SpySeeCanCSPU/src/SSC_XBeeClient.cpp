/*
    SSC_XBeeClient.cpp
*/
#pragma once


#include "SSC_XBeeClient.h"
#define DEBUG
#include "DebugCSPU.h"
#define DBG_DIAGNOSTIC 1
#define DBG_STREAM 0
#define DBG_SEND 1


bool SSC_XBeeClient::send(const SSC_Record& record, int timeOutCheckResponse) {
  // Beware of byte alignment. Only even adresses can be adressed directly
  // in SAMD 16-bit architecture.
  DPRINTSLN(DBG_SEND, "SSC_XBeeClient::send(record) called");

  uint8_t buffer[sizeof(SSC_Record) + 2];
  buffer[0] = SSC_RecordType;
  buffer[1] = 0;
  memcpy(buffer + 2, &record, sizeof(record));

#ifdef XBEECLIENT_INCLUDES_UTILITIES
#ifdef DBG_PRINT_OUTGOING_FRAMES
  if (displayOutgoingFramesOnSerial) {
    displayFrame((uint8_t *)  buffer, (uint8_t) (sizeof(record) + 2));
  }
#endif
#endif
  return XBeeClient::send((uint8_t *)  buffer, (uint8_t) (sizeof(record) + 2), timeOutCheckResponse);
}

void SSC_XBeeClient::openStringMessage(uint8_t recordType) {
  DPRINTS(DBG_STREAM, "SSC_XBeeClient::openStringMessage(), type= ");
  DPRINTLN(DBG_STREAM, (uint8_t) recordType);
  XBeeClient::openStringMessage( (uint8_t) recordType);
}

bool SSC_XBeeClient::getDataRecord(SSC_Record& record, const uint8_t* data, uint8_t dataSize) {
  if (dataSize != (sizeof(SSC_Record) + 2)) {
    DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent size. size=");
    DPRINT(DBG_DIAGNOSTIC, dataSize);
    DPRINTS(DBG_DIAGNOSTIC, ", SSC_Record size=");
    DPRINTLN(DBG_DIAGNOSTIC, sizeof(SSC_Record));
    return false;
  }
  if (data[0] != (uint8_t) SSC_RecordType) {
    DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent record type (");
    DPRINT(DBG_DIAGNOSTIC, data[0]);
    DPRINTS(DBG_DIAGNOSTIC, ", expected ");
    DPRINTLN(DBG_DIAGNOSTIC, (uint8_t) SSC_RecordType);
    return false;
  }
  memcpy(&record, data + 2, sizeof(record));
  return true;
}

bool SSC_XBeeClient::getString(char* str, const uint8_t* data, uint8_t dataSize) {
  bool result = false;

  if (dataSize <= 2) {
    DPRINTS(DBG_DIAGNOSTIC, "*** Error: received empty string");
    *str = '\0';
    return result;
  }
  const char* s = (const char*) (data + 2);

  switch (data[0]) {
#ifdef UNSUPPORTED_IN_SPYSEECAN
    case (uint8_t) SSC_RecordType::StatusMsg:
    case (uint8_t) SSC_RecordType::CmdRequest:
    case (uint8_t) SSC_RecordType::CmdResponse:
    case (uint8_t) SSC_RecordType::StringPart:
      if (dataSize != (uint8_t) strlen(s) + 3) {
        // 2 bytes + string + '\0'
        DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent size. size=");
        DPRINT(DBG_DIAGNOSTIC, dataSize);
        DPRINTS(DBG_DIAGNOSTIC, ", String size, from third byte=");
        DPRINTLN(DBG_DIAGNOSTIC, strlen(s));
      } else {
        memcpy(str, data + 2, strlen(s) + 1);
        result = true;
      }
      break;
#endif
    default:
      DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent record type (");
      DPRINT(DBG_DIAGNOSTIC, data[0]);
      DPRINTS(DBG_DIAGNOSTIC, ", expected StatusMsg, CmdRequest or CmdResponse");
      break;
  } // switch
  return result;
}

#ifdef XBEECLIENT_INCLUDES_UTILITIES

void SSC_XBeeClient::displayFrame(uint8_t* data, uint8_t dataSize)
{
  unsigned char type = data[0];
  SSC_Record rec;
  const char* s;
  Serial << "--- Frame content ---" << ENDL;
  switch (type) {
    case (int) SSC_RecordType:
      Serial << "DataRecord message" << ENDL;
      if (getDataRecord(rec, data, dataSize)) {
        rec.print(Serial);
        Serial << ENDL << "  --- End of record ---" << ENDL;
      }
      break;
    default:
      Serial << "*** Unsupported type: " << type << ENDL;
  }
  Serial << "--- End of frame content ---" << ENDL;
}

bool SSC_XBeeClient::send(  uint8_t* data,
                            uint8_t dataSize,
                            int timeOutCheckResponse,
                            uint32_t SH, uint32_t SL) {
  DPRINTSLN(DBG_STREAM, "SSC_XBeeClient::send() ");
  if (displayOutgoingFramesOnSerial) {
    displayFrame(data, dataSize);
  }
  return XBeeClient::send(data, dataSize, timeOutCheckResponse, SH, SL);
};
#endif
