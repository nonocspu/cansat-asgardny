#include "SSC_GPS_Client.h"
#include "Adafruit_GPS.h"

#ifdef __AVR__
#include "SoftwareSerial.h"
SoftwareSerial mySerial(3 , 2);
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
HardwareSerial &mySerial = Serial1;
#endif
const bool detailedOutput = true; // Set to true to print all values on Serial
SSC_GPS_Client g(mySerial);
SSC_Record rec;

void setup() {
  DINIT(115200);
  Serial << "calling gps.begin..." << ENDL;
  g.begin();
  Serial << "Setup ok. " << ENDL;
}

void loop() {
  static unsigned long lastMillis = millis();
  rec.clear();
  rec.timestamp = millis();
  g.readData(rec);

  unsigned long delta = rec.timestamp - lastMillis;
  if (detailedOutput) {
    Serial.print(rec.timestamp); Serial.print(":  Longitude:"); Serial.print(rec.GPS_LongitudeDegrees , 10); //prints the different parameters sent by the gps
    Serial.print(", Latitude:"); Serial.print(rec.GPS_LatitudeDegrees , 10);
    Serial.print(", Altitude:"); Serial.print(rec.GPS_Altitude , 10);
    Serial << "  DeltaT =" << delta << ENDL; Serial.println(rec.newGPS_Measures);

  }
  if (rec.GPS_LatitudeDegrees < 45) Serial << "*** Abnormal value of latitude ***" << ENDL;
  if (rec.GPS_LongitudeDegrees < 3) Serial << "******** Abnormal Value of longitude *******" << ENDL;
  lastMillis = rec.timestamp;


  delay(10); //test possibilities


}
