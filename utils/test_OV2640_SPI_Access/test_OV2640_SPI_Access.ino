/* 
 *  Just SPI communication to camera
 */

#define DEBUG
#include "DebugCSPU.h"
#include <SPI.h>
#include "ArduCAM.h" 

const byte Camera_CS = 10;
ArduCAM myCAM(OV2640, Camera_CS);
SPISettings camSettings(4000000, MSBFIRST, SPI_MODE0);

void checkSPI_Bus() {
  uint8_t temp;
  while (1) {
    //Check if the ArduCAM SPI bus is OK
    SPI.beginTransaction(camSettings);
    myCAM.write_reg(ARDUCHIP_TEST1, 0x55);
    temp = myCAM.read_reg(ARDUCHIP_TEST1);
    SPI.endTransaction();
    Serial <<  "SPI: Wrote 0x55... Read again: 0x";
    Serial.println( temp, HEX);
    if (temp != 0x55) {
      Serial << "SPI interface Error! Retrying..." << ENDL;
      delay(1000);
      continue;
    } else {
      Serial << "SPI interface OK." << ENDL;
      break;
    }
  }
}

void setup() {
  DINIT(115200)
  SPI.begin();
  Serial << "Testing: CS=" << Camera_CS << ENDL;
  checkSPI_Bus();
}

void loop() {

}
